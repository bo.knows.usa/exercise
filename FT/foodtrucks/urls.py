"""foodtrucks URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import patterns, include, url
from django.contrib import admin
from location import views
from location import api_views

urlpatterns = patterns('', 
    url(r'^admin/', include(admin.site.urls)),
)

api_urls = patterns('foodtruck/api/v1',
    url(r'/locations/$', api_views.LocationList.as_view({'get': 'list'}), name='location-list'),
    url(r'/locations/(?P<id>[0-9]+)$', api_views.LocationDetail.as_view(), name='location-detail'),
    url(r'/locationstatus/(?P<id>[0-9]+)$', api_views.LocationStatusDetail.as_view(), name='status-detail'),
    url(r'/nearbytrucks/$', api_views.NearbyTrucks.as_view({'get': 'list'}), name='nearby-list'),
)

urlpatterns += patterns('',
    url(r'^foodtruck/api/v1', include(api_urls)),
)
