from django.shortcuts import render
from rest_framework import status, generics
from serializers import LocationSerializer, LocationStatusSerializer
from models import Location, LocationStatus
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from helpers import get_nearby_trucks

class LocationStatusDetail(generics.RetrieveAPIView):
    model = LocationStatus
    queryset = LocationStatus.objects.all()
    serializer_class = LocationStatusSerializer
    lookup_field = 'id' 


class LocationDetail(generics.RetrieveAPIView):
    model = Location
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    lookup_field = 'id' 


class LocationList(ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    

class NearbyTrucks(ModelViewSet):
    serializer_class = LocationSerializer

    def get_queryset(self):
        queryset = Location.objects.all()
        latitude = self.request.query_params.get('latitude', None)
        longitude = self.request.query_params.get('longitude', None)
        radius = self.request.query_params.get('radius', 1) # Default to 1KM
        try:
            latitude, longitude, radius = float(latitude), float(longitude), float(radius)
        except:
            latitude = longitude = radius = None            

        if latitude and longitude:
            location_ids = get_nearby_trucks(latitude, longitude, radius) 
            return queryset.filter(id__in=location_ids)
        else:
            return queryset.none() 
