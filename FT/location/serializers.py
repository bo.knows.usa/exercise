from rest_framework import serializers
from models import Location, LocationStatus
from django.db.models import Prefetch


class LocationStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = LocationStatus
        lookup_field = 'id'
	fields = '__all__'


class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        lookup_field = 'id'
        depth = 1
	fields = '__all__'
