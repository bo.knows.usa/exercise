from django.contrib import admin
from location.models import LocationStatus, Location, FacilityType

# Register your models here.

@admin.register(LocationStatus)
class LocationStatusAdmin(admin.ModelAdmin):
    model = LocationStatus
    list_display = ('id', 'name',)
    search_fields = ['name', 'id']
    extra = 1
    ordering = ('id',)


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    model = Location 
    list_display = ('id', 'applicant', 'facility_type', 'latitude', 'longitude', 'expriation_date')
    search_fields = ['applicant', 'facility_type__name']
    extra = 1
    ordering = ('id',)


@admin.register(FacilityType)
class FacilityTypeAdmin(admin.ModelAdmin):
    model = FacilityType
    list_display = ('id', 'name',)
    search_fields = ['name', 'id']
    extra = 1
    ordering = ('id',)
