from django.db import connection

def run_sql(sql):
    """Execute a raw sql and return result
    Args:
        sql(string): raw sql
    Resutns:
        sql result
    """
    cursor = connection.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    result = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in data]]

    return result


def get_nearby_trucks(latitude, longitude, radius):
    """Function return a list of food trucks within a certain distance around a location 
    Args:
        latitude(float): latitude of a location
        longitude(float): longitude of a location
        radius(float): radius that we want to search(in KM)
    Returns:
        list of location ids 
    """

    sql = '''
        select id from location where sqrt(  
            (  
             (({1}-longitude)*PI()*12656*cos((({0}+latitude)/2)*PI()/180)/180)  
             *  
             (({1}-longitude)*PI()*12656*cos ((({0}+latitude)/2)*PI()/180)/180)  
            )  
            +  
            (  
             (({0}-latitude)*PI()*12656/180)  
             *  
             (({0}-latitude)*PI()*12656/180)  
            )  
        ) <= {2}
        '''.format(latitude, longitude, radius)

    result = run_sql(sql)
    
    return [location['id'] for location in result]
