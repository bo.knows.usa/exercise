#!/usr/bin/env python 
#encoding: utf-8 
import sys
from enum import Enum


class TriangleType(Enum):
	NONE_TRIANGLE = -1
	EQUILATERAL = 1
	ISOSCELES = 2
	SCALENE = 3

	
class Triangle:
	def __init__(self, laterals):
		self.laterals = laterals
		self.sorted_laterals = sorted(self.laterals)
	
	def is_triangle(self):
		if len(self.sorted_laterals) == 3:
			first_lateral, second_lateral, third_lateral = self.sorted_laterals
			if first_lateral > 0:			
				return True if first_lateral + second_lateral > third_lateral else False
			else:
				return False
		else:
			return False
					
	def get_type(self):
		if self.is_triangle():
			if self.sorted_laterals[0] == self.sorted_laterals[2]:
				return TriangleType.EQUILATERAL
			elif self.sorted_laterals[0] == self.sorted_laterals[1] or self.sorted_laterals[1] == self.sorted_laterals[2]:
				return TriangleType.ISOSCELES
			else:
				return TriangleType.SCALENE
		else:
			return TriangleType.NONE_TRIANGLE