#!/usr/bin/env python
#encoding: utf-8

import unittest
import Triangle
class mytest(unittest.TestCase):

	def setUp(self):
		self.tclass = Triangle.Triangle
		
	def tearDown(self):
		pass
		
	def testBoolean(self):
		self.assertEqual(self.tclass([-1, 0, 1]).is_triangle(), False)
		self.assertEqual(self.tclass([2, 2, 3]).is_triangle(), True)
		self.assertEqual(self.tclass([2, 2, 5]).is_triangle(), False)
		self.assertEqual(self.tclass([6, 7, 8]).is_triangle(), True)
		self.assertEqual(self.tclass([2, 3]).is_triangle(), False)
		self.assertNotEqual(len(self.tclass([2, 3, 3, 4]).laterals), 3)
		self.assertNotEqual(len(self.tclass([3, 3]).laterals), 3)
		
	def testType(self):
		self.assertEqual(self.tclass([2, 2, 3]).get_type(), Triangle.TriangleType.ISOSCELES)
		self.assertEqual(self.tclass([1, 2, 3]).get_type(), Triangle.TriangleType.NONE_TRIANGLE)
		self.assertEqual(self.tclass([3, 4, 5]).get_type(), Triangle.TriangleType.SCALENE)
		self.assertEqual(self.tclass([3, 3, 3]).get_type(), Triangle.TriangleType.EQUILATERAL)
		self.assertEqual(self.tclass([-1, 2, 3]).get_type(), Triangle.TriangleType.NONE_TRIANGLE)		

		
if __name__ == '__main__':
	unittest.main()