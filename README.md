This repository includes two small exercises for now.

1. Triangle problem: 
    Give a triangle, return whether the triangle is equilateral, isosceles or scalene.
    
2. Food Trucks:
    Create a service that tells the user what types of food trucks might be found near a specific location on a map. 
    There are two files, one is the main class and the other is the unittest.
    
    Example:
    http://121.42.33.201/foodtruck/api/v1/nearbytrucks/?latitude=37.77487132&longitude=-122.3985317&radius=0.3
    or
    http://121.42.33.201/foodtruck/api/v1/nearbytrucks/?format=json&latitude=37.77487132&longitude=-122.3985317&radius=0.3

    There are three parameters, latitude(required), longitude(required) and radius(in KM, optional and default 1KM).
    
    It could be polished such as adding the ordering by distance. But I have limited time so I didn't do that.